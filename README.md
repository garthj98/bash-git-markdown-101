# Bash, git, bitbucket and Markdown Intro ðŸ™†â€â™‚ï¸

This is a lesson to introduce the follown technologies that we'll be using:

- git
- bitbucket
- bash
- markdown (.md)

### Oh-my-zsh
Oh my zsh is a framework for managing zsh configurations, it helps by:

- Colour coding files and folder names to ease visual identifcation

### Bash commands
Bash is the language used in the terminal on macOS and 

``` bash
# Return current directory path 
$ pwd
# Return what is in the current directory
$ ls 
$ ll
$ ls -l
$ ls -a
# Change directory
$ cd <folder>
# Go up one folder level
$ cd ..
# Create a folder
$ mkdir <folder>
# Create a file
$ touch <file>
# Delete a file
$ rm <file>
# Delete a folder
$ rm -rf
# Clear terminal 
$ clear
# Open folder 
$ open
# Write in a document 
$ echo '<text>' > <file>
# Return what is in a document 
$ cat <file>
# Current directory 
$ .
# Previous directory 
$ ..
# Change name
$ mv <oldname> <newname>
```

### Git commands
A git tracks the changes between versions of a decoument 

``` bash
# Start a git repo
$ git init
# Add file contents to the index
$ git add
# Record changes to the repository
$ git commit
# Check status of git
$ git status
# Check the git log 
$ git log
```

### Bitbucket
Is a place where all versions of deocuments that have been commited live in the cloud.

- How to add a remote and push to remote repo on bitbucket.
- Navigate to the desired folder.
- Start a git repository `$ git init`.
- Add a remote by creating a new repositary on bitbucket.
- Then using the command `$ git remote add origin <your_remote_URL>`.
- Check this connection has been established by using `$ git remote --v`.
- Add and commit the documents in the folder using `$ git add .` and `$ git commit -m '<message>'`.
- To upload the gits to bitbucket use command `git push origin master`.

### Markdown

# Title 1

## Smaller Title 2

### Subtitle 1

#### Suttitble 2

### lists
- list2 
- list1 

### ordered lsit 
1. one
2. two

### show code on a markdown 

code box 
```
code
code
code
```

code in text 
this is `code`

**bold type**

__bold type__

*italic*

<u>underline</u>